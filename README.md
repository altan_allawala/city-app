# README #

An app we created as a project for a graduate-level statistical computing course at Brown University. This app, implemented in R using Shiny, predicts the best U.S. city for an individual to live, given their preferences. The app takes inputs for several preferences such as desired population size, weather, economic preferences and diversity requirements and returns a ranking of the top 5 cities. It also returns a map of these cities and a downloaded list of available job opportunities and salaries, as scraped from indeed.com.

Data is taken from official sources, mainly the US Census Bureau.

Copyright (c) 2016 Altan Allawala, Aderonke Ilegbusi, Sandra Agik, Jonathan Miao.